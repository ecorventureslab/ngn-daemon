# NGN Daemon

This utility runs background processes on Windows, Mac, and Linux.

### Windows

Should work on Windows XP and higher, including server editions.

On Windows, creating a NGN Daemon will create a Windows service,
complete with basic system health logging to the Application event log. 
This requires elevated administrative permissions.

### Mac & Linux

A daemon is created along with a `pid` file. A basic system health log
(syslog) is stored in /var/logs/<daemon_name>.log. On some systems,
this action may require `sudo`.

## Usage

```js
var daemon = require('ngn-daemon');

var name = 'My Daemon',
	app	 = '/path/to/myapp.js';

// Create and/or start a daemon.
daemon.start(name,app); 

// Restart the daemon
daemon.restart(name,app);

// Stop the daemon
daemon.stop(name);

// Remove the daemon (uninstall)
daemon.remove(name);
```

Please note that `start` and `restart` have two optional additional
arguments:

	daemon.start(name,app, <pidfile>, <callback>);
and
	daemon.restart(name,app, <pidfile>, <callback>);

To provide a `callback` method without a `pidfile`, set `pidfile=null`.
