/*var daemon = require("daemonize2").setup({
    main: "app.js",
    name: "sampleapp",
    pidfile: "sampleapp.pid"
});*/
var util = require('ngn-util'),
    exec = require('child_process').exec,
    path = require('path'),
    os = require('os'),
    nodeExe = '',
    windows = (os.platform().indexOf('win') >= 0),
    EventLog = null,
    _EventLog = null,
    isPrivileged = null;

// Check for proper permissions
var isAdmin = function(callback){
  if (isPrivileged !== null){
    if (isPrivileged){
      console.log("Requires elevated administrator privileges.");
    }
    callback();
  }
  if (windows) {
    exec("whoami /groups | findstr /c:\"S-1-5-32-544\" | findstr /c:\"Enabled group\"", function(err, r){
      if(r.length === 0){
        isPrivileged = false;
        callback();
        process.exit(1);
      } else {
        isPrivileged = true;
        callback();
      }
    });
  }
};

// Generic Logger
var logger = {
  logger: function(log){
    if (windows && EventLog == null){
      EventLog = new _EventLog(log);
    }
    return EventLog;
  },
  info: function(logfile,msg){
    logger.logger(logfile).log(msg,'Information');
  },
  error: function(logfile,msg){
    msg = msg || 'Unknown Error';
    logger.logger(logfile).log(msg,'Error');
  },
  warn: function(logfile,msg){
    logger.logger(logfile).log(msg,'Warning');
  }
};

// Retrieve the node binary
var getNodeExe = function(){
  if (nodeExe !== ''){
    return nodeExe;
  }
  for (var i=0;i<process.argv.length;i++){
    if (process.argv[i].indexOf('nodes')){
      nodeExe = process.argv[i];
      return nodeExe;
    }
  }
};

// Check for permission errors
var checkPermError = function(error){
  if (error.message.indexOf('Administrator access') >= 0){
    logger.error('Permission Denied. Administrative Access Required.');
    process.exit(1);
  } else {
    logger.error(error.toString());
    process.exit(1);
  }
};

if (windows) {
  var nssm = path.join(__dirname,'..','bin','win'+(os.arch().indexOf('64')>0 ? '64':'32'),'nssm.exe');
  //isAdmin(function(){
    _EventLog = require('windows-eventlog').EventLog;
  //});
} else {
  //TODO: Add log4js event logging.
}


// Daemon functionality.
var daemon = {
  
  // Create and/or start a service.
  start: function(svcName,file,pidfile,callback){
    callback = callback || function(){};
    // Support MS Windows
    if (windows){
      if (svcName == undefined || svcName == null){
        throw "A name for the service is required.";
      }
      exec('net start "'+svcName+'"',function(err,stdout,stderr){
        if (err){
          if (err.code == 2){
            if (err.message.indexOf('already been started') >= 0 && err.message.indexOf('service name is invalid') < 0){
              logger.warn(svcName,'An attempt to start the service failed because the service is already running. The process should be stopped before starting, or the restart method should be used.');
              callback();
              return;
            } else if (err.message.indexOf('service name is invalid') < 0){
              console.log(err);
              console.log(">>>>>",svcName,'---->',file);
              return;
            }

            if (file == undefined || file == null){
              throw "No file provided.";
            }
            
            // Construct the service command
            var cmd = nssm+' install "'+svcName+'" "'+getNodeExe()+'" "'+file+'"';
            exec(cmd,function(error,stdout,stderr){
              if (error){
                checkPermError(error);
              } else if (stderr.trim().length > 0){
                logger.error(svcName,stderr);
              } else {
                daemon.start(svcName,file,pidfile,callback);
              }
            });
          } else {
            logger.error(svcName,err.toString());
          }
        } else {
          logger.info(svcName,'Started Successfully.');
          callback();
        }
      })
    }
  },
  
  // Stop an existing service.
  stop: function(svcName,callback){
    callback = callback || function(){};
    if (windows){
      exec('net stop "'+svcName+'"',function(err,stdout,stderr){
        if (err){
          if (err.code == 2){
            logger.warn(svcName,'An attempt to stop the service failed because the service is/was not running.');
            callback();
            return;
          } else {
            checkPermError(err);
          }
        } else if (stderr.trim().length > 0){
          logger.error(svcName,stderr);
        } else {
          logger.info(svcName,'Successfully Stopped.');
          callback();
        }
      });
    }
  },
  
  // Restart an existing service
  restart: function(svcName,file,pidfile){
    daemon.stop(svcName,function(){
      daemon.start(svcName,file,pidfile)
    });
  },
  
  // Uninstall the service
  remove: function(svcName,callback){
    callback=callback||function(){};
    if (windows){
      isAdmin(function(){
        if (!isPrivileged){
          throw 'Permission Denied. Requires administrative privileges.';
        }
        daemon.stop(svcName,function(){
          console.log(nssm+' remove "'+svcName+'" confirm');
          var cmd = nssm+' remove "'+svcName+'" confirm';
          console.log('Removing '+svcName+' service.');
          exec(cmd,function(error,stdout,stderr){
            if (error){
              switch (error.code){
                case 3:
                  console.log(svcName+' could not be found.');
                  break;
                case 4:
                  console.log('Service is running.');
                  console.log('Stopping '+svcName+'...');
                  daemon.stop(svcName,function(){
                    daemon.remove(svcName,callback);
                  });
                  break;
                default:
                  console.log(error);
              }
              checkPermError(error);
            } else if (stderr.trim().length > 0){
              console.log('Error: ',stderr);
              logger.error(svcName,stderr);
            } else {
              logger.warn(svcName,'Service Removed.');
              callback();
            }
          });
        });
      });
    }
  }
};

// Export functionality for the module.
module.exports = daemon;

//module.exports.remove('NGN Tester');
//module.exports.start('NGN Tester');
//module.exports.restart('NGN Test',path.join(__dirname,'app.js'));
var pth = "C:\\Users\\Corey\\AppData\\Roaming\\npm\\node_modules\\ngn-mechanic\\lib\\mechanic\\app.js";
console.log('Huh?');
/*module.exports.remove('NGN-Mechanic',function(){
  console.log(pth);
  console.log(require('fs').existsSync(pth));
});*/

module.exports.start('NGN Mechanic',pth);
//module.exports.remove('NGN Mechanic');
//module.exports.remove('NGN Test');
//module.exports.remove('NGN Test');
//module.exports.start('NGN Test',path.join(__dirname,'app.js'));
